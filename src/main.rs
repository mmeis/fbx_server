#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use]
extern crate rocket;
extern crate rocket_contrib;
#[macro_use]
extern crate serde_derive;

use std::fs;
use rocket::Outcome;
use rocket::http::Status;
use rocket::response::NamedFile;
use rocket::request::{self, Request, FromRequest};

use rocket_contrib::json::Json;
use serde::Serialize;

use std::collections::HashMap;

#[get("/")]
fn index() -> &'static str {
    "Hello, world!"
}

#[derive(Serialize)]
struct FbxCmdResult<T: Serialize>
{
    success: bool,
    result: T,
}

#[derive(Serialize)]
struct LoginResult
{
    logged_in: bool,
    challenge: String,
}

#[get("/api/v6/login")]
fn login()
    -> Json<FbxCmdResult<LoginResult>>
{
    Json(FbxCmdResult {
        success: true,
        result: LoginResult {
            logged_in: true,
            challenge: "VzhbtpR4r8CLaJle2QgJBEkyd8JPb0zL".into()
        }})
}

#[derive(Serialize)]
struct LoginSessionResult
{
    session_token: String,
    challenge: String,
    permissions: HashMap<String, bool>,
}

#[derive(Deserialize)]
struct LoginSessionPayload
{
    app_id: String,
    password: String,
}

#[post("/api/v6/login/session", data="<payload>")]
fn login_session(payload: Json<LoginSessionPayload>)
    -> Json<FbxCmdResult<LoginSessionResult>>
{
    Json(FbxCmdResult {
        success: true,
        result: LoginSessionResult {
            session_token: "SomeSessionToken".into(),
            challenge: "SomeChallenge".into(),
            permissions: [("explorer".into(), true)].iter().cloned().collect(),
        }})
}

#[derive(Serialize)]
struct FbxFileInfo
{
    path: String,
    name: String,
    mimetype: String,
    r#type: String,
    size: i32,
    modification: u64,
    index: i32,
    link: bool,
    target: String,
    hidden: bool,
    foldercount: i32,
    filecount: i32,
}

struct AuthToken(String);

#[derive(Debug)]
enum AuthTokenError {
    NoAuthentication,
    BadAuthentication,
}

impl<'a, 'r> FromRequest<'a, 'r> for AuthToken {
    type Error = AuthTokenError;

    fn from_request(request: &'a Request<'r>) -> request::Outcome<Self, Self::Error> {
        let keys: Vec<_> = request.headers().get("X-Fbx-App-Auth").collect();

        match keys.len() {
            0 => Outcome::Failure((Status::Unauthorized, AuthTokenError::NoAuthentication)),
            1 => {
                println!("Coucou :: {}", keys[0]);
                Outcome::Success(AuthToken(keys[0].to_string()))
            },
            _ => Outcome::Failure((Status::BadRequest, AuthTokenError::BadAuthentication)),
        }
    }
}

#[get("/api/v6/fs/info/<file_path>")]
fn fs_info(_token: AuthToken, file_path: String)
    -> Json<FbxCmdResult<FbxFileInfo>>
{
    let name: String = match file_path.as_ref() {
        "L3Rlc3QubXAz" => "test.mp3".into(),
        "L3RvdG8ubXAz" => "toto.mp3".into(),
        "L3RydWMubXAz" => "truc.mp3".into(),
        "L2Rpci90ZXN0Lm1wMw==" => "test.mp3".into(),
        "L2Rpcg==" => "dir".into(),
        _ => "/".into()
    };
    let size: i32 = match file_path.as_ref() {
        "L3Rlc3QubXAz" => 6668854,
        "L3RvdG8ubXAz" => 3065529,
        "L3RydWMubXAz" => 8887960,
        "L2Rpci90ZXN0Lm1wMw==" => 6668854,
        _ => 4096,
    };
    let ftype: String = match file_path.as_ref() {
        "Lw==" => "dir".into(),
        "L2Rpcg==" => "dir".into(),
        _ => "file".into()
    };

    Json(FbxCmdResult {
        success: true,
        result: FbxFileInfo {
            path: file_path,
            name: name,
            mimetype: "audio/mpeg".into(),
            r#type: ftype,
            size: size,
            modification: 0,
            index: 1,
            link: false,
            target: "".into(),
            hidden: false,
            foldercount: 5,
            filecount: 5,
        }})
}

#[get("/api/v6/fs/ls/<file_path>")]
fn fs_ls(_token: AuthToken, file_path: String)
    -> Json<FbxCmdResult<[FbxFileInfo; 4]>>
{
    if file_path == "L2Rpcg==" {
        return Json(
            FbxCmdResult {
                success: true,
                result: [
                    FbxFileInfo {
                        path: "L2Rpci90ZXN0Lm1wMw==".into(),
                        name: "test.mp3".into(),
                        mimetype: "audio/mpeg".into(),
                        r#type: "file".into(),
                        size: 6668854,
                        modification: 0,
                        index: 1,
                        link: false,
                        target: "".into(),
                        hidden: false,
                        foldercount: 5,
                        filecount: 5,
                    },
                    FbxFileInfo {
                        path: "L2Rpci90ZXN0Lm1wMw==".into(),
                        name: "test.mp3".into(),
                        mimetype: "audio/mpeg".into(),
                        r#type: "file".into(),
                        size: 6668854,
                        modification: 0,
                        index: 1,
                        link: false,
                        target: "".into(),
                        hidden: false,
                        foldercount: 5,
                        filecount: 5,
                    },
                    FbxFileInfo {
                        path: "L2Rpci90ZXN0Lm1wMw==".into(),
                        name: "test.mp3".into(),
                        mimetype: "audio/mpeg".into(),
                        r#type: "file".into(),
                        size: 6668854,
                        modification: 0,
                        index: 1,
                        link: false,
                        target: "".into(),
                        hidden: false,
                        foldercount: 5,
                        filecount: 5,
                    },
                    FbxFileInfo {
                        path: "L2Rpci90ZXN0Lm1wMw==".into(),
                        name: "test.mp3".into(),
                        mimetype: "audio/mpeg".into(),
                        r#type: "file".into(),
                        size: 6668854,
                        modification: 0,
                        index: 1,
                        link: false,
                        target: "".into(),
                        hidden: false,
                        foldercount: 5,
                        filecount: 5,
                    },
                ]
            }
        )
    }
    else {
        Json(
            FbxCmdResult {
                success: true,
                result: [
                    FbxFileInfo {
                        path: "L2Rpcg==".into(),
                        name: "dir".into(),
                        mimetype: "dir".into(),
                        r#type: "dir".into(),
                        size: 1024,
                        modification: 0,
                        index: 1,
                        link: false,
                        target: "".into(),
                        hidden: false,
                        foldercount: 0,
                        filecount: 1,
                    },
                    FbxFileInfo {
                        path: "L3Rlc3QubXAz".into(),
                        name: "test.mp3".into(),
                        mimetype: "audio/mpeg".into(),
                        r#type: "file".into(),
                        size: 6668854,
                        modification: 0,
                        index: 1,
                        link: false,
                        target: "".into(),
                        hidden: false,
                        foldercount: 5,
                        filecount: 5,
                    },
                    FbxFileInfo {
                        path: "L3RvdG8ubXAz".into(),
                        name: "toto.mp3".into(),
                        mimetype: "audio/mpeg".into(),
                        r#type: "file".into(),
                        size: 3065529,
                        modification: 0,
                        index: 1,
                        link: false,
                        target: "".into(),
                        hidden: false,
                        foldercount: 5,
                        filecount: 5,
                    },
                    FbxFileInfo {
                        path: "L3RydWMubXAz".into(),
                        name: "truc.mp3".into(),
                        mimetype: "audio/mpeg".into(),
                        r#type: "file".into(),
                        size: 8887960,
                        modification: 0,
                        index: 1,
                        link: false,
                        target: "".into(),
                        hidden: false,
                        foldercount: 5,
                        filecount: 5,
                    },
                ]
            }
        )
    }
}

#[get("/api/v6/dl/<file_path>")]
fn fs_dl(_token: AuthToken, file_path: String) -> NamedFile
{
    println!("{}", file_path);

    match file_path.as_ref() {
        "L3Rlc3QubXAz" => NamedFile::open("test.mp3"),
        "L3RvdG8ubXAz" => NamedFile::open("toto.mp3"),
        "L3RydWMubXAz" => NamedFile::open("truc.mp3"),
        "L2Rpci90ZXN0Lm1wMw==" => NamedFile::open("test.mp3"),
        _ => NamedFile::open("truc.mp3"),
    }.unwrap()
}


fn main()
{
    rocket::ignite()
        .mount("/",routes![
               index,
               login,
               login_session,
               fs_info,
               fs_ls,
               fs_dl,
        ])
        .launch();
}
